


# 2023-02-06 Hotwire Progressive Enhancement

Many developers incorrectly assume [Everyone has JavaScript, right? 5](https://www.kryogenix.org/code/browser/everyonehasjs.html).

I’ve dealt with bugs reported by customers where a JavaScript heavy application was bricked because of a hard-to-reproduce state of the application or deployed bug in the code.

I’ve also dealt with bugs reported by customers where the application did not work at all because their firewall incorrectly flagged one line of code from a 3rd party library as potentially malicious and blocked the entire JavaScript, which crippled the entire application.

However, even if you don’t subscribe to the basic [definition of progressive enhancement 1](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement), there are many more benefits to the hotwire approach of progressive than running on browsers where JavaScript is not currently working.

## The resilient approach (or progressive enhancement) from the [hotwire/stimulus handbook 1](https://stimulus.hotwired.dev/handbook/designing-for-resilience):

> This resilient approach, commonly known as *progressive enhancement*, is the practice of delivering web interfaces such that the basic functionality is implemented in HTML and CSS, and tiered upgrades to that base experience are layered on top with CSS and JavaScript, progressively, when their underlying technologies are supported by the browser.

## DHH, Hotwire creator/contributor

> **When is a Hi-Fi experience necessary?**
>
> Part of the ethos of Hotwire is…“just because you can, doesn’t mean you should…”. You are hurting the web when you are bathing it in unnecessary JavaScript. It’s a progressive enhancement, it’s a ladder…Most pages do not need all of that. A bunch of applications would be totally fine if the only thing you did was Turbo Drive. They’d already feel fast enough, they’d already be great
>
> What you want to do is identify the 5 or 10% of your app where you really want the Hi-Fi experience, there you pay 100%, you build it and it takes more time. For the other 90 to 95%, you get it on sale.

## Sam Stephenson, Hotwire creator/contributor

> If the web offers us guidance, take it. The web gives us `<a>` to change pages and `<form>` to change server-side state, so that’s what we’ll use in our app. That way we have fewer decisions to make and we’re responsible for less code.
>
> If we want to do something that can’t be done with HTML and CSS alone, we’ll still try to do as much as we can with HTML and CSS, and use a layer of JavaScript on top for the rest.

## Hotwire Progress Enhancements

A great example of progressive enhancement in Hotwire is [Thinking in Hotwire: Progressive Enhancement | Boring Rails 7](https://boringrails.com/articles/thinking-in-hotwire-progressive-enhancement/)

## Personal Opinion

Until Hotwire, it’s been a long time since feeling I had the library of tools to build a *performant* and *stable* application in the velocity we are building. For me, the benefits of progressive enhancement are:

- Development Velocity
- Application performance
- Stability (with all business logic on backend)
- Cross Platform (web and mobile app)
- And last as a side benefit, much (not all) of your application still works without JavaScript.


---

#ruby

---

- [Everyone has JavaScript, right?](https://www.kryogenix.org/code/browser/everyonehasjs.html)
- [Thinking in Hotwire: Progressive Enhancement | Boring Rails 7](https://boringrails.com/articles/thinking-in-hotwire-progressive-enhancement/)
- [Dreaming Bigger with Rails — David Heinemeier Hansson answers our questions about Hotwire | Blog WizVille 1](https://www.wizville.com/en/blog/dhh-interview-hotwire)
- [Hotwire vs htmx - comparison - #2 by sam - Hotwire Discussion 4](https://discuss.hotwired.dev/t/hotwire-vs-htmx-comparison/1614/2)
- [hotwire/stimulus handbook 1](https://stimulus.hotwired.dev/handbook/designing-for-resilience):
- [Progressive Enhancement - MDN Web Docks 1](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement)