# 2023-05-03 Gitlab Markdown Image Dimensions

Every day I capture screenshots, copy to clipboard and then pasting to Gitlab markdown.  I love how it can automatically upload the image and write the markdown at the same time.  However, with higher resolution screens the pasted image often ends up too big for the documentation making it difficult to read the text around the image.  To workaround this issue, you have the following options:

## Option 1: Resize Image First

You can save the image to disk or paste it into an image editor, resize the image, and then add to Gitlab markdown. While this works and follows "best practice" for most web pages  (see Image Size Best Practice below), it's a cumbersome multi-step process.

## Option 2: Use Collapsible Section

In Gitlab markdown you can wrap the image in a [collapsible section](https://docs.gitlab.com/ee/user/markdown.html?_gl=1*1vpipzd*_ga*MTA2NzYxMjI5OC4xNTYwMTk3NjE2*_ga_ENFH3X7M5Y*MTY4MzEyMTU4Mi44OS4wLjE2ODMxMjE1ODYuMC4wLjA.#collapsible-section) where you click a link to expand in order to reveal the image. 

```markdown
<details><summary>Click to expand</summary>
![alt text](uploads/screenshot.png "Title Text")
</details>
```

This is simpler than the resize image process and makes it easier to read the surrounding text, but you lose the information the image communicates unless you open the collapsible section and the image is still big.

## Option 3: Change Image Dimensions in Markdown

[GitLab 15.7](https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/#change-the-dimensions-of-images-in-markdown) (December 2022) added official support for [changing image dimensions in markdown](https://docs.gitlab.com/ee/user/markdown.html?_gl=1*1ler957*_ga*MTA2NzYxMjI5OC4xNTYwMTk3NjE2*_ga_ENFH3X7M5Y*MTY4MzEyMTU4Mi44OS4xLjE2ODMxMjE2NDYuMC4wLjA.#change-the-image-or-video-dimensions).

```markdown
![alt text](img/screenshot.png "Title Text"){width=100}

![alt text](img/screenshot.png "Title Text"){width=100 height=100px}

![alt text](img/screenshot.png "Title Text"){width=75%}
```

This is the simplest approach above the others above and now my preferred approach.

TIP: in gitlab the user can also click the image to see the larger/original version.

## Image Dimension Attributes

A few items to note about image dimension attributes:

- **Pixels:** The `width` or `height` attribute specifies size in pixels.  So writing `width=100px` is the same as `width=100`.
- **Percentage:** An image percentage applies to total space available, not the original size of the image.  So, if you wanted an image to be one quarter the width of the content area, you could set width to 25%.  I do not recommend using percentage as it doesn't always translate well between desktop and mobile unless you are using media queries to resize.
- **Auto Proportions:** When specifying just `width` or `height` of the image (but not both) the browser will automatically calculate the the other dimension after downloading the image and maintain proportions.  Much easier to specify just one dimension

## Image Size Best Practice

It's important to note that the following best practice items are good advice for images on most web pages, but can be ignore for  issues/documentation in gitlab markdown (in my opinion)

- Best practice suggests not to downsize smaller images for web pages as larger images require more bandwidth while the page is displaying smaller images.
- Best practice suggests to specify both width and height of an image so that browser reserves the space where the image will load before the it download the image.  This avoids page jumping.

---

#markdown #gitlab

---

- [Gitlab Markdown Documentation > Collapsible Section](https://docs.gitlab.com/ee/user/markdown.html?_gl=1*1vpipzd*_ga*MTA2NzYxMjI5OC4xNTYwMTk3NjE2*_ga_ENFH3X7M5Y*MTY4MzEyMTU4Mi44OS4wLjE2ODMxMjE1ODYuMC4wLjA.#collapsible-section) 
- [Gitlab Markdown Documentation > Image Dimensions](https://docs.gitlab.com/ee/user/markdown.html?_gl=1*1ler957*_ga*MTA2NzYxMjI5OC4xNTYwMTk3NjE2*_ga_ENFH3X7M5Y*MTY4MzEyMTU4Mi44OS4xLjE2ODMxMjE2NDYuMC4wLjA.#change-the-image-or-video-dimensions)