


# 2023-03-09 Animation Speed

We've learned how to make [Performant and Smooth CSS Animations](2021-10-07 Smooth CSS Animations.md) animation.  Animations on web pages can enhance user experience.

- Highlight the background of a new element on the screen to bring attention
- Fade element of item before removing it from screen to make the transition of removing the element less jaring
- etc

Using the wrong speed for the animation can actually decrease the user experience as they feel the site is slow or sluggish.  From the Neilson Norman Group write that [100ms is perceived as instant, and 1 second is considered the upper limit of a user’s flow of thought](https://www.nngroup.com/articles/response-times-3-important-limits/). 

## How fast should your UI animations be?

An [article on valhead.com](https://valhead.com/2016/05/05/how-fast-should-your-ui-animations-be/) provides some good advice

> Good animation timing is more of an art than a science. Thinking in terms of a range instead of one set duration value will serve you much better in your design work.
>
> 200ms to 500ms seconds is a good range to start with for interface animations.

They also provide a good graph for helping how fast to make a UI animation.

![timing range](assets/2023-03-09 Animation speed/timing-range-1.png)

Ultimately, after setting the speed... test and use your best judgement on the speed an how a user might perceive the animation.

## TailwindCSS — Transition Duration 

TailwindCSS includes [transition duration utility classes](https://tailwindcss.com/docs/transition-duration) allows you to set different speeds for different elements, and the same transition/speed may not work with all elements.

---

#ux #tailwindcss #browser #performance #animation

---

#ruby

---

- [Smooth CSS Animations.md](2021-10-07 Smooth CSS Animations.md) 

- [How fast should your UI animations be?](https://valhead.com/2016/05/05/how-fast-should-your-ui-animations-be/)

- [100ms is perceived as instant, and 1 second is considered the upper limit of a user’s flow of thought](https://www.nngroup.com/articles/response-times-3-important-limits/)

-   [TailwindCSS transition duration utility classes](https://tailwindcss.com/docs/transition-duration)