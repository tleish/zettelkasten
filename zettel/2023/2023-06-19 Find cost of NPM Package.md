# 2023-06-19 Find cost of NPM Package

In keeping a performant application you want to understand the cost of adding a new javascript library at time of the library evaluation.  It can be difficult to find both the unzipped and zipped sizes of a package.  Some size/performance conscientious javascript library authors include the size directly in the description (e.g. "~4kb minified, ~2kb gzipped").  However, this is not always the case.  You must then result to find the size in other methods.

## Method 1: NPM Package Page
[npmjs.com](https://www.npmjs.com/) does have a section for "Unpacked Size" to provide the unzipped size of the package.  However, it does not provide the zipped size.

## Method 2: Chrome Dev Tools
If you can find a demo size using the package, Chrome Dev Tools provides a way to find the zipped size of a package.  In the Network tab, you can filter by "JS" and then find the package.  The "Size" column provides the zipped size of the package.  The downside is that you must find a demo page that uses the package.

## Method 3: Bundlephobia
[bundlephobia.com](https://bundlephobia.com/) is a great resource for finding the size of a package.  It provides both the unzipped and zipped sizes.  It also provides a visual representation of the package size over time.  The other advantage to using this site is you can include a direct link in tickets or other documentation.

e.g. https://bundlephobia.com/package/pristinejs@1.1.0

![image-20230618103611747](assets/2023-06-19 Find cost of NPM Package/image-20230618103611747.png)




---

#javascript #npm #package #performance

---

- [bundlephobia.com](https://bundlephobia.com/)
- [npmjs.com](https://www.npmjs.com/)



