# 2021-10-22 Stimulus Event Listeners

You must be careful when adding listeners to stimulus controllers.  For example:

```javascript
export default class extends Controller {
  connect() {
    window.addEventListener('resize', (event) => this.layout(event));
  }
}
```

Active event listeners prevent garbage collection of all variables captured within its scope. Once created, the event listener will remain in memory until:

- explicitly removed with `removeEventListener()`
- the associated DOM element is removed.

Even if your usage of the stimulus controller removes the DOM element, future usages of the controller may not.  Design controllers to safely remove event listeners with one of the following methods:

## Stimulus Actions (preferred)

Actions are how you handle DOM events in HTML.  You can dispatch events in other controllers and listen to them in the current controller.

```html
<div data-controller="gallery" 
     data-action="some-controller:loadImages->gallery#layout">
</div>
```

Note: dispatch events within the DOM only exist with the children of the DOM tree.  If trying to find events outside DOM tree, you must look for them in the global event scope with `@window` or `@document`

```html
<div data-controller="gallery"
     data-action="resize@window->gallery#layout">
</div>
```



## Stimulus Disconnect

If you cannot use a stimulus action to handle events (preferred), when be sure to cleanup the event on disconnect of the controller.

```javascript
export default class extends Controller {
  connect() {
    window.addEventListener('resize', this.layout);
  }

  disconnect() {
    window.removeEventListener('resize', this.layout);
  }
}
```

See: https://stimulus.hotwired.dev/reference/actions

## Call Once

There might be some situations where you want to add the event listener and then remove it within the same action.  You can do this using the once [parameter](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#parameters):

```javascript
element.addEventListener(event, func, { once: true });
```

For example:

```javascript
// event_listener_controller.js
export default class extends Controller {
  static values = { target: String, eventType: String };
  
  dispatchOnce(_event) {
    let listenerFunction = () => this.dispatch(this.eventTypeValue);
    const element = document.getElementById(this.targetValue);
    return !element.addEventListener(this.eventTypeValue, listenerFunction, { once: true });
  }
}
```

In the above controller, we assign an event listener to a target element and then dispatch a function once it completes.  However, we only want it to fire once.  You could use this to fire something after an animation completes.

```html
<button data-controller="reveal event event-listener link"
        data-reveal-hide-class="slide-closed"
        data-event-listener-target-value="left-menu"
        data-event-listener-event-type-value="transitionend"
        data-action="click->reveal#hide click->event-listener#dispatchOnce click->event#preventDefault event-listener:transitionend->link#visit"
>Click</button>
```

The above example [runs a smooth animation using css transition](2021-10-07 Smooth CSS Animations.md) on a menu and then fires `link#visit` on transitioned event.  This is useful when using turbo-strada library where a screenshot is taken on navigation change.  If it's taken before the animation finishes, it can look odd when navigating back.

Note that IE appears to be the only browser not supporting the `once` parameter.  If IE support is need, the follow function could be added to auto-remove the event listener when executed:

```javascript
function addEventListenerOnce(target, type, listener, addOptions, removeOptions) {
    target.addEventListener(type, function fn(event) {
        target.removeEventListener(type, fn, removeOptions);
        listener.apply(this, arguments);
    }, addOptions);
}
```




---

#javascript #hotwired #stimulus #events #eventListener #animation

---

- [Stimulus Reference - Actions](https://stimulus.hotwired.dev/reference/actions)
- [EventTarget.addEventListener() - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#parameters)
- [2021-10-07 Smooth CSS Animations.md](2021-10-07 Smooth CSS Animations.md) 
