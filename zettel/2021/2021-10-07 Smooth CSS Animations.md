# 2021-10-07 Smooth CSS Animations

When making smooth CSS animations, you need understand [Browser Rendering Performance](2021-09-21 Browser Rendering Performance.md) and challenges with [Minimize Browser Reflow](2021-09-27 Minimize Browser Reflow.md).  Browsers can animate four styles smoothly without causing browser reflow:

- **Position** — transform: translateX(*n*) translateY(*n*) translateZ*(n*);
- **Scale** — transform: scale(*n*);
- **Rotation** — transform: rotate(*n*deg);
- **Opacity** — opacity: *n*;

### Progress Bar Animation Example

Hover over a [TailwindCSS Progress Bar example](https://play.tailwindcss.com/CbQMrAc6PE) to see a smoothly animation using scale (transform):

```html
<div class="w-full bg-gray-200 h-2">
  <div class="bg-green-500 h-2 transform transition-transform origin-left scale-x-50 hover:scale-x-100"></div>
```

---

#ux #tailwindcss #browser #reflow #performance

---

- [Smooth as Butter: Achieving 60 FPS Animations with CSS3](https://medium.com/outsystems-experts/how-to-achieve-60-fps-animations-with-css3-db7b98610108)
- [TailwindCSS Progress Bar example](https://play.tailwindcss.com/CbQMrAc6PE) 
- [2021-09-21 Browser Rendering Performance](2021-09-21 Browser Rendering Performance.md) 
- [2021-09-27 Minimize Browser Reflow](2021-09-27 Minimize Browser Reflow.md) 

```ruby
test = 1
```