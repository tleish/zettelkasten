# 2021-09-27 Batch Edit HTML Element

To [Minimize Browser Reflow](2021-09-27 Minimize Browser Reflow.md) you want to update the DOM in batches using one of the following techniques.

## Original

```javascript
var element = document.getElementById('example-element');
element.style.opacity = '0.5'; // triggers reflow
element.style.padding = '20px 10px'; // triggers reflow
element.style.width = '200px'; // triggers reflow
```

## fastdom

```javascript
var element = document.getElementById('example-element');
window.fastdom.mutate(() => {
  element.style.opacity = '0.5';
  element.style.padding = '20px 10px';
  element.style.width = '200px';
}); // trigger reflow
```

With a single change, this is not as important.  However, with multiple changes all at once [fastdom](https://github.com/wilsonpage/fastdom) can drastically improve performance.

You don't need use fastdom on everything, just some things.  Best way to determine which items to use fastdom on is Google Chrome > Developer Tools > Performance > [CPU Throttling](https://developer.chrome.com/docs/devtools/evaluate-performance/#simulate_a_mobile_cpu) = 6x slowdown.


---

#ux #tailwindcss #browser #reflow #performance #fastdom

---

- [What forces layout/reflow. The comprehensive list. · GitHub](https://gist.github.com/tleish/adf004e92527ca88a5b019c0449b8ba2)
- [GitHub - wilsonpage/fastdom: Eliminates layout thrashing by batching DOM measurement and mutation tasks](https://github.com/wilsonpage/fastdom)
- [2021-09-27 Minimize Browser Reflow](2021-09-27 Minimize Browser Reflow.md)
- [Analyze runtime performance - Chrome Developers #Simulate a mobile CPU](https://developer.chrome.com/docs/devtools/evaluate-performance/#simulate_a_mobile_cpu)

