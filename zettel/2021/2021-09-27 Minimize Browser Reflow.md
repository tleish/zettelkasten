# 2021-09-27 Minimize Browser Reflow

When [Browsers Render a Page](2021-09-21 Browser Rendering Performance.md)  changing the DOM with Javascript/CSS after initial loading can cause *reflow*, where the browser has to recalculate position and dimensions of multiple elements on the page.  This reflow can often make the page feel sluggish.

Things you can do to minimize browser reflow are:

- Minimize DOM depth/nesting
- Update CSS classes at the leafs of a DOM tree, vs the root
- Use absolute or fixed position on animated elements
- [Update element in batches](2021-09-27 Batch Edit HTML Elements.md).
- Analyze repaint/reflow performance using browser tools

---

#ux #tailwindcss #browser #reflow #performance

---

- [Repaints and Reflows: Manipulating the DOM responsibly - Get Snippet of Code](https://sites.google.com/site/getsnippet/javascript/dom/repaints-and-reflows-manipulating-the-dom-responsibly)
- [10 Ways to Minimize Reflows and Improve Performance - SitePoint](https://www.sitepoint.com/10-ways-minimize-reflows-improve-performance/)
- [What forces layout/reflow. The comprehensive list. · GitHub](https://gist.github.com/tleish/adf004e92527ca88a5b019c0449b8ba2)
- [2021-09-21 Browser Rendering Performance](2021-09-21 Browser Rendering Performance.md)  
- [2021-09-27 Batch Edit HTML Elements](2021-09-27 Batch Edit HTML Elements.md) 
- [2021-10-07 Smooth CSS Animations](2021-10-07 Smooth CSS Animations.md) 

