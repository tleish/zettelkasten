# 2021-11-04 Avoid SQL OFFSET

MySQL and other databases make it easy to paginate using LIMIT and OFFSET.  

## PERFORMANCE PROBLEMS

This is a simple solution when the total paginating records are in the hundreds.  However, if your software EXPLODES and some of your users might have MILLIONS of records, this will quickly become a problem.

### OFFSET 0 of 1,000,000 RECORDS

```sql
SELECT * FROM shipments 
WHERE user_id = 123 
ORDER BY shipped_at 
LIMIT 10 OFFSET 0
```

FAST (0.4ms on 2020 MacBook): The database will load 10 records from the above query and then return 10 records

### OFFSET 900,000 of 1,000,000 RECORDS

```sql
SELECT * FROM shipments 
WHERE user_id = 123 
ORDER BY shipped_at 
LIMIT 10 OFFSET 800000
```

SLOW (617ms on 2020 MacBook): The database will load 800,010 records from the above query and then return the last 10 records

The 2nd query is 1,500 times slower than the 1st even though we are returning 10 records with both.  This may seem trivial, but when you have thousands or millions of users sending more half second queries or more to your database at the same time during peak then your entire system gets bogged down.

![img](assets/2021-11-04 Avoid SQL OFFSET/offset_slow_query_mysql.png)

image from: [Why Order By With Limit and Offset is Slow - Faster Pagination in Mysql](https://www.eversql.com/faster-pagination-in-mysql-why-order-by-with-limit-and-offset-is-slow/)

## PAGINATION PROBLEMS

The other problem with using offset is when pagination through records where the result set might change dynamically.  For example, the original pagination set includes 1,000 records.  However, another user adds/updates/deletes record(s) and changes the result set while you are on page 2.  When you click next, your offset is different than before and you either see the same record as the prior page, or the new page skips record(s).

## SOLUTION: Seek/Keyset pagination

Keyset pagination is a technique where you use an INDEXED column (created_at in this case) as a key to paginate through records based on the last record.

> Note: some developers/libraries incorrectly refer to *keyset* pagination as *cursor* pagination, which is incorrect and different pagination technique

Here's the general steps/idea for keyset pagination

1. Using the `shipped_at` value of the last record, we get all records greater than that date.

```diff
SELECT * FROM shipments 
WHERE user_id = 123 
+	AND shipped_at > '2018-08-05 02:05:19'
ORDER BY shipped_at 
LIMIT 10
```

2. However, this can skip any records which share the same date.  So we insert another condition which also includes the last ID.

```diff
SELECT * FROM shipments 
WHERE user_id = 123 
+	(
	  AND shipped_at > '2018-08-05 02:05:19'
+	  OR (shipped_at = '2018-08-05 02:05:19' AND id > 800123)
+ )
ORDER BY shipped_at 
LIMIT 10
```

3. Finally, we guarantee the sort order by adding ID

```diff
SELECT * FROM shipments 
WHERE user_id = 123 
	(
	  AND shipped_at > '2018-08-05 02:05:19'
	  OR (shipped_at = '2018-08-05 02:05:19' AND id > 800123)
  )
- ORDER BY shipped_at 
+ ORDER BY shipped_at, id
LIMIT 10
```

The "keyset" above is set of keys (`shipped_at` and `id`) to do the pagination.

### Why not just use ID?

If it makes sense, just use the ID.  However, in this scenario the shipped_at and id might not be in the same order.

### Performance

The above query on the same record set now takes approx 0.4ms (on 2020 MacBook), the same speed as the `LIMIT 10 OFFSET 0`.  This speed is always the same no matter where the query is executed.

![img](assets/2021-11-04 Avoid SQL OFFSET/offset_vs_seek_method_slow_order_by_limit.png)

image from: [Why Order By With Limit and Offset is Slow - Faster Pagination in Mysql](https://www.eversql.com/faster-pagination-in-mysql-why-order-by-with-limit-and-offset-is-slow/)

>  Note: The performance of keyset pagination depends on using a database index and the number of `ORDER BY` columns
### PAGINATION ACCURACY

This approach is more accurate in pagination strategy.  If the pagination results during pagination, the pages are more accurate (see PAGINATION PROBLEMS above).


---

#ruby #sql #pagination #performance

---

- [OFFSET is bad for skipping previous rows](https://use-the-index-luke.com/sql/partial-results/fetch-next-page)
- [Why Order By With Limit and Offset is Slow - Faster Pagination in Mysql](https://www.eversql.com/faster-pagination-in-mysql-why-order-by-with-limit-and-offset-is-slow/)
- [Value Based Pagination in Ruby on Rails](https://naturaily.com/blog/value-based-pagination-rails)
- [Keyset pagination | GitLab](https://docs.gitlab.com/ee/development/database/keyset_pagination.html)
  - [lib/gitlab/pagination](https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/pagination)
  - [config/initializers/active_record_keyset_pagination.rb](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/config/initializers/active_record_keyset_pagination.rb)
  - [spec/lib/gitlab/pagination](https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/spec/lib/gitlab/pagination)
- [Gem: rb_pager](https://github.com/BambangSinaga/rb_pager)
- [Gem: pagy-cursor](https://github.com/Uysim/pagy-cursor)
- [2021-11-04 Avoid SQL OFFSET](https://gitlab.com/uqueue/resources/zettelkasten/-/blob/main/2021-11-04 Avoid SQL OFFSET.md)
