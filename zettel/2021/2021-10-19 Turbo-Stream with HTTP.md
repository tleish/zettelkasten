# 2021-10-19 Turbo-Stream with HTTP

## Myth: Turbo-streams only work with WebSockets

Many developers incorrectly believe that turbo-streams only work with WebSockets.  While turbo-streams shine with WebSockets, turbo-streams can also be easily combined with HTTP requests.

<table>
  <tr>
    <td colspan="2">1. Turbo sends a <i>fetch request</i> to the server with:</td>
  </tr>
  <tr>
    <td>...`POST`, `PUT`, `PATCH`, or `DELETE`</td>
    <td>...turbo-frame `GET`.</td>
  </tr>
  <tr>
    <td colspan="2">2. The server responds with html:</td>
  </tr>
  <tr>
    <td>
<pre style="color:#000000;background:#ffffff;"><span style="color:#7f0055; ">&lt;</span><span
    style="color:#7f0055; ">turbo-stream</span> action=<span style="color:#2a00ff; ">"append"</span> target=<span
    style="color:#2a00ff; ">"messages"</span><span style="color:#7f0055; ">&gt;</span>
  <span style="color:#7f0055; ">&lt;</span><span style="color:#7f0055; ">template</span><span
      style="color:#7f0055; ">&gt;</span>
    <span style="color:#7f0055; ">&lt;</span><span
      style="color:#7f0055; font-weight:bold; ">div</span> id=<span style="color:#2a00ff; ">"message_1"</span><span
      style="color:#7f0055; ">&gt;</span>
    	 This div will be appended.
    <span style="color:#7f0055; ">&lt;/</span><span
      style="color:#7f0055; font-weight:bold; ">div</span><span style="color:#7f0055; ">&gt;</span>
  <span style="color:#7f0055; ">&lt;/</span><span style="color:#7f0055; ">template</span><span
      style="color:#7f0055; ">&gt;</span>
<span style="color:#7f0055; ">&lt;/</span><span style="color:#7f0055; ">turbo-stream</span><span
      style="color:#7f0055; ">&gt;</span>
</pre>
    </td>
    <td>
<pre style="color:#000000;background:#ffffff;"><span style="color:#7f0055; ">&lt;</span><span style="color:#7f0055; ">turbo-frame</span> id=<span style="color:#2a00ff; ">"turbo_frame_message_1"</span><span style="color:#7f0055; ">&gt;</span>
  <span style="color:#7f0055; ">&lt;</span><span style="color:#7f0055; ">turbo-stream</span> action=<span style="color:#2a00ff; ">"append"</span> target=<span style="color:#2a00ff; ">"messages"</span><span style="color:#7f0055; ">&gt;</span>
    <span style="color:#7f0055; ">&lt;</span><span style="color:#7f0055; ">template</span><span style="color:#7f0055; ">&gt;</span>
      <span style="color:#7f0055; ">&lt;</span><span style="color:#7f0055; font-weight:bold; ">div</span> id=<span style="color:#2a00ff; ">"message_1"</span><span style="color:#7f0055; ">&gt;</span>
        This div will be appended.
      <span style="color:#7f0055; ">&lt;/</span><span style="color:#7f0055; font-weight:bold; ">div</span><span style="color:#7f0055; ">&gt;</span>
    <span style="color:#7f0055; ">&lt;/</span><span style="color:#7f0055; ">template</span><span style="color:#7f0055; ">&gt;</span>
  <span style="color:#7f0055; ">&lt;/</span><span style="color:#7f0055; ">turbo-stream</span><span style="color:#7f0055; ">&gt;</span>
<span style="color:#7f0055; ">&lt;/</span><span style="color:#7f0055; ">turbo-frame</span><span style="color:#7f0055; ">&gt;</span>
</pre>
    </td>
  </tr>
  <tr>
    <td colspan="2">3. Turbo appends the turbo-stream HTML
    </td>
  </tr>
  <tr>
    <td>...to the document.</td>
    <td>...inside the matching turbo-frame#id.</td>
  </tr>
  <tr>
    <td colspan="2">4. Turbo detects the DOM change includes a turbo-stream. It processes the
      turbo-stream action (append, replace, delete, etc.) and removes the turbo-stream template from the DOM
      (so it doesn't process it again).
    </td>
  </tr>
</table>





---

#javascript #hotwired #browser #turbo #turbo-stream #turbo-frame

---

- [Turbo Handbook - Streams](https://turbo.hotwired.dev/handbook/streams)
- [Turbo Handbook - Frames](https://turbo.hotwired.dev/handbook/frames)
- [2021-09-27 Batch Edit HTML Elements.md](2021-09-27 Batch Edit HTML Elements.md) 
