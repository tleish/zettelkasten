# 2021-09-21 Browser Rendering Performance

Browsers render a page in 5 steps: DOM > Styles > Layout > Paint > Composite

- Step 1: **DOM** —  initial load or JavaScript changes
- Step 2: **Styles** — calculate which style rules to apply to an element (e.g. inherit parent font-face, override parent style font-face, etc)
- Step 3: **Layout** — how much space and where to place it on the screen based on content, width, height, margin, padding, border, etc. (most expensive step)
- Step 4: **Paint** — painting the [color](2021-09-20 tailwind css color.md) pixels (text, borders, backgrounds, [shadows](2021-09-20 tailwind css shadow color.md), etc)
- Step 5: **Composite** — position overlapping layers

The *Layout* step takes the longest on browsers.  Changes to DOM or Styles often trigger Layout Reflows, which can affect webpage performance.

---

#ux #tailwindcss #browser #reflow #performance

---

- [Rendering Performance  |  Web Fundamentals  |  Google Developers](https://developers.google.com/web/fundamentals/performance/rendering)
- [2021-09-20 tailwind css color](2021-09-20 tailwind css color.md) 
- [2021-09-20 tailwind css shadow color](2021-09-20 tailwind css shadow color.md) 

