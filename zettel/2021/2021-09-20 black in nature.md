# 2021-09-20 Black in Nature

You might notice that the [TailwindCSS palette](2021-09-20 tailwind css color.md) does not include black (`#000000`).  This is because as many designers know, [true black does not really exist in nature](https://www.adobe.com/creativecloud/design/discover/is-black-a-color.html).  Instead, you should use dark colors like gray-900 (`#18181B`) or blueGray-900 (`#04172A`) if using the 22 color palette. 

---

#ux #tailwindcss #color

---

- [Are black and white colors? | Adobe](https://www.adobe.com/creativecloud/design/discover/is-black-a-color.html)
- [Customizing Colors - Tailwind CSS](https://tailwindcss.com/docs/customizing-colors)
- [2021-09-20 tailwind css color](2021-09-20 tailwind css color.md) 

