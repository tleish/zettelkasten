# 2021-09-20 Tailwind CSS Shadow Color

TailwindCSS does not yet support shadow colors, which is a consideration practice for a more [beautiful CSS Box Shadow](2021-09-16 tailwind css beautiful box shadows.md).  Having a shadow color match the background [mimics real-world colors](https://learnui.design/blog/color-in-ui-design-a-practical-framework.html) better than black partially transparent color.

Fortunately there's some [discussion](https://github.com/tailwindlabs/tailwindcss/issues/654) around adding this feature to TailwindCSS.

---

#ux #tailwindcss #shadow #color

---

- [[Feature Proposal] Coloured Box Shadow · Issue #654 · tailwindlabs/tailwindcss · GitHub](https://github.com/tailwindlabs/tailwindcss/issues/654)
- 2021-09-16 Tailwind CSS Beautiful Box Shadows](2021-09-16 tailwind css beautiful box shadows.md) 
- [Why Color Theory Sucks (& what framework to use instead)](https://learnui.design/blog/color-in-ui-design-a-practical-framework.html)
- [2021-09-20 tailwind css color](2021-09-20 tailwind css color.md) 

