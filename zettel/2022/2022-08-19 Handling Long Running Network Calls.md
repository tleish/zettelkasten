- 2022-08-16 Handling Long Running Network Calls

  A recent article on ideas for improving the Ruby JIT compiler sparked a conversation between devs in Hackernews discussing different approaches to making a Ruby on Rails application run faster.  If a Rails app is CPU bound, then improving the JIT might help.  However, if the Rails app is primarily I/O bound, then this approach might not make much of a difference.

  A former Shopify developer wrote:

  > I worked at Shopify for several years. The big problems were *always* due to I/O. Usually that was in the form of long-running network calls. You're calling the FedEx API for shipping rates in the cart. All of a sudden FedEx starts taking 5 seconds instead of 1 second, and all of your worker capacity evaporates. That kind of thing.
  >
  > [Hacker News reply](https://news.ycombinator.com/item?id=32411553)

  The discussion mentions a couple of solutions:

  #### Solution 1: Handle all 3rd party network requests via job workers

  Using job workers for 3rd party requests works well to offset app server capacity.  The downside is the delay in for real-time request expectations.  In addition, the application still risks tying up job worker capacity for slow network calls.  Job worker capacity isn't always as critical as web app capacity, but can be.

  #### Solution 2: Writing proxy services in other languages that could parallelize

  This approach avoids tying up worker capacity.  However, now your application stack includes yet another language burden for developers to manage.

  #### Solution 3: Create an asynchronous ruby proxy server

  The one solution not mentioned is creating an asynchronous ruby proxy server (e.g. [Goliath](https://github.com/postrank-labs/goliath) or [Falcon](https://github.com/socketry/falcon)).  This avoids tying up worker capacity and also maintains a single Ruby language stack.

  Long term I see [Falcon](https://github.com/socketry/falcon) and Rails evolving to allow a single asynchronous server, instead of splitting into two services once ActiveRecord works with fibers.

  > The only caveat is that it doesn't work with Ruby on Rails, because `ActiveRecord` doesn't support `async` gem. You can still use it with Rails if `ActiveRecord` is not involved.
  >
  > See: [Async Ruby - Bruno Sutic](https://brunosutic.com/blog/async-ruby)

  In response to the above article, @ioquatix responded:

  > ActiveRecord does work but it’s hugely limited because they have explicit per-thread resource pools which we can’t get around very easily.
  >
  > See:  [Hackernews discussion](https://news.ycombinator.com/item?id=29050482) 

  ---

  #html #forms

  ---

  - [Faster Ruby: Thoughts from the outside | Hacker News](https://news.ycombinator.com/item?id=32408716)
  - [Hacker News reply](https://news.ycombinator.com/item?id=32411553)
  - [goliath: a non-blocking Ruby web server framework](https://github.com/postrank-labs/goliath)
  - [falcon: A high-performance web server for Ruby, supporting HTTP/1, HTTP/2 and TLS.](https://github.com/socketry/falcon)
  - [Falcon > Rails Integration](https://socketry.github.io/falcon/guides/rails-integration/index.html)
  - [Async Ruby - Bruno Sutic](https://brunosutic.com/blog/async-ruby)
  - [ActiveRecord does work but it’s hugely limited because they have explicit per-th... | Hacker News](https://news.ycombinator.com/item?id=29051742)
  - [YouTube: Don't Wait For Me! Scalable Concurrency for Ruby 3! / Samuel Williams @ioquatix](https://www.youtube.com/watch?v=Y29SSOS4UOc&t=787s)

- https://singlebrook.com/2013/05/28/use-ruby-s-source_location-to-find-where-a-method-is-defined/)
