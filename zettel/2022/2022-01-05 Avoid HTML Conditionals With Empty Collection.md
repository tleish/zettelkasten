# 2022-01-05 Avoid HTML Conditionals With Empty Collection

A common pattern in ERB files is to render a message if the collection is empty.

```erb
<h1>Search Results:</h1>
<ul id="content">
  <% if @orders.empty? %>
    <li>No results found</li>
  <% else %>
		<% @orders.each do |order| %>
  		<li><%= @order.name %></li>
	  <% end %>  
  <% end %>  
</ul>
```

Removing conditionals simplifies complexity.  Fortunately, CSS has a class that allows us to show the same results to users without using a conditional `:only-child`:

```erb
<style>
  .only-child-hidden {
    display: none;
  }
  
  .only-child-hidden:only-child {
    display: inline;
  }
</style>

<h1>Search Results:</h1>
<ul id="content">
  <% @orders.each do |order| %>
  		<li><%= @order.name %></li>
	<% end %> 
  <li class="only-child-hidden">No results found</li>
</ul>
```

Code is now much simpler.

### TaildwindCSS `only` Modifier

Using the TailwindCSS we default the item to hidden and then use the `only` modifier to assign `:only-child`  to inline:

```erb
<h1>Search Results:</h1>
<ul id="content">
  <% @orders.each do |order| %>
  		<li><%= @order.name %></li>
	<% end %> 
  <li class="hidden only:inline">No results found</li>
</ul>
```

### Ideas

- Show no results when collection empty
- Show hints or next steps (e.g. button to "add order") when collection empty

---

#erb #css #tailwindcss

---

- [:only-child - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:only-child)
- [Tailwind CSS - only](https://tailwindcss.com/docs/hover-focus-and-other-states#only)
- [Twitter - Matt Swanson](https://twitter.com/_swanson/status/1456653563011211268)

