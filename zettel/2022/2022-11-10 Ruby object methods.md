


# 2022-11-10 Filtering Ruby Object Methods

Sometimes you want to find methods of a specific ruby object.  You can search the internet, but the documentation isn't always clear.  You already have the instance variable defined, but you are trying to figure out that one method.

## Scenario

Here's an example of how coding sometimes plays out within a Rails app.

> ME: I remember rails has a method to join an array into an English, something about a sentance.

```ruby
items = %w[one two three]
items.sentance
#=> undefined method `sentance' for ["one", "two", "three"]:Array (NoMethodError)
```

> ME: Hm, that's not it and no hints from the [did_you_mean gem](https://github.com/ruby/did_you_mean).  I remember it was something to do with sentence.  I'll search for it.

```ruby
items = %w[one two three]
items.methods.grep(/sentance/)
#=> []
```

> ME:  I must've remembered wrong.  What are all the methods available for this object?

```ruby
items = %w[one two three]
items.methods
#=> [:to_h, :include?, :&, :*, :+, :-, :at, :fetch, :last, :union, :difference, :intersection, :push, :append, :pop, :shift, :unshift, :each_index, :join, :rotate, :rotate!, :sort!, :sort_by!, :collect!, :map!, :select!, :filter!, :keep_if, :values_at, :delete_at, :delete_if, :reject!, :transpose, :fill, :assoc, :rassoc, :uniq!, :compact, :compact!, :flatten, :flatten!, :shuffle!, :shuffle, :sample, :permutation, :combination, :repeated_permutation, :repeated_combination, :product, :bsearch, :sort, :bsearch_index, :deconstruct, :count, :find_index, :select, :filter, :reject, :collect, :map, :first, :all?, :any?, :one?, :none?, :minmax, :to_sentence, :|, :reverse_each, :zip, :take, :take_while, :drop, :<=>, :<<, :cycle, :drop_while, :to_formatted_s, :==, :to_default_s, :sum, :uniq, :[], :[]=, :to_fs, :insert, :to_xml, :empty?, :eql?, :index, :rindex, :replace, :clear, :max, :min, :inspect, :length, :size, :each, :reverse, :concat, :prepend, :reverse!, :to_ary, :to_a, :to_s, :pack, :blank?, :delete, :slice, :slice!, :to_param, :dig, :to_query, :hash, :each_slice, :each_cons, :each_with_object, :chunk, :slice_before, :slice_after, :slice_when, :chunk_while, :to_set, :chain, :lazy, :find, :entries, :sort_by, :grep, :grep_v, :detect, :find_all, :filter_map, :flat_map, :collect_concat, :inject, :reduce, :partition, :group_by, :tally, :min_by, :max_by, :minmax_by, :member?, :each_with_index, :each_entry, :present?, :presence, :dup, :itself, :yield_self, :then, :taint, :tainted?, :untaint, :untrust, :untrusted?, :trust, :frozen?, :methods, :singleton_methods, :protected_methods, :private_methods, :public_methods, :instance_variables, :instance_variable_get, :instance_variable_set, :instance_variable_defined?, :remove_instance_variable, :instance_of?, :kind_of?, :is_a?, :tap, :class, :display, :singleton_class, :clone, :public_send, :method, :public_method, :singleton_method, :define_singleton_method, :extend, :to_enum, :enum_for, :===, :=~, :!~, :nil?, :respond_to?, :freeze, :object_id, :send, :__send__, :!, :!=, :__id__, :equal?, :instance_eval, :instance_exec]
```

> ME: Ugh!  Finding a needle in a haystack.  I includes all the root object methods.  Can I narrow it down to array specific methods?

```ruby
items = %w[one two three]
items.methods - Object.instance_methods
#=> [:to_h, :include?, :&, :*, :+, :-, :at, :fetch, :last, :union, :difference, :intersection, :push, :append, :pop, :shift, :unshift, :each_index, :join, :rotate, :rotate!, :sort!, :sort_by!, :collect!, :map!, :select!, :filter!, :keep_if, :values_at, :delete_at, :delete_if, :reject!, :transpose, :fill, :assoc, :rassoc, :uniq!, :compact, :compact!, :flatten, :flatten!, :shuffle!, :shuffle, :sample, :permutation, :combination, :repeated_permutation, :repeated_combination, :product, :bsearch, :sort, :bsearch_index, :deconstruct, :count, :find_index, :select, :filter, :reject, :collect, :map, :first, :all?, :any?, :one?, :none?, :minmax, :to_sentence, :|, :reverse_each, :zip, :take, :take_while, :drop, :<<, :cycle, :drop_while, :to_formatted_s, :to_default_s, :sum, :uniq, :[], :[]=, :to_fs, :insert, :to_xml, :empty?, :index, :rindex, :replace, :clear, :max, :min, :length, :size, :each, :reverse, :concat, :prepend, :reverse!, :to_ary, :to_a, :pack, :delete, :slice, :slice!, :dig, :each_slice, :each_cons, :each_with_object, :chunk, :slice_before, :slice_after, :slice_when, :chunk_while, :to_set, :chain, :lazy, :find, :entries, :sort_by, :grep, :grep_v, :detect, :find_all, :filter_map, :flat_map, :collect_concat, :inject, :reduce, :partition, :group_by, :tally, :min_by, :max_by, :minmax_by, :member?, :each_with_index, :each_entry]
```

> ME: At this point I could start searching the internet, but I have one more snippet to try.  I could select only methods which include a source location.  If it's a core ruby class method, the location returns `nil`.  Rails and my own project classes will return a file path and line number.

```ruby
items = %w[one two three]
items.methods.select { |name| items.method(name).source_location }
#=> [:to_sentence, :to_formatted_s, :to_fs, :to_xml, :pack, :to_param, :to_query, :to_set, :present?, :presence]
```

> ME: Ah, the method I'm looking for is `#to_sentence`.  I also see that I misspelled "sentence".  Let's try an earlier approach, but with the correct spelling.

```ruby
items = %w[one two three]
items.methods.grep(/sentence/)
#=> [to_sentence]
```

> ME: Much simpler if I remembered how to spell.  Now, let's test #to_sentence method

```ruby
items = %w[one two three]
items.to_sentence
#=> one, two, and three
```



## Class Methods

The same principles could be applied to class or instance methods

```ruby
# class methods
Array.methods - Object.methods
# or 
Array.methods.select { |name| Array.method(name).source_location }
#=> [:yaml_tag, :json_creatable?, :to_param, :to_query, :present?, :blank?, :presence, :to_yaml]

# instance methods from class
Array.instance_methods - Object.instance_methods
# or 
Array.instance_methods.select { |name| Array.instance_method(name).source_location }
#=> [:to_sentence, :to_formatted_s, :to_fs, :to_xml, :pack, :to_param, :to_query, :to_set, :present?, :presence, :to_yaml]
```



## Summary

You've now learned multiple approaches to finding methods.  

- **`Array#grep` on a methods array to search for a class**
  best to use when you remember part of the method name
- **Subtract `Object.methods` or `Object.instance_methods`**
  best when finding methods for classes with fewer methods (e.g. not Array)
- **Select methods with  `#source_location`**
  Best for large monkey-patched class like Rails#Array


---

#ruby

---

- [List Ruby Object Methods](https://inspirnathan.com/posts/21-list-ruby-object-methods/)
- [to_sentence (Array) - APIdock](https://apidock.com/rails/Array/to_sentence)
- [Use ruby's `source_location` to find where a method is defined](https://singlebrook.com/2013/05/28/use-ruby-s-source_location-to-find-where-a-method-is-defined/)