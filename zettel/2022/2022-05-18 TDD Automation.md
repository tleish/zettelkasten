

# 2022-05-18 TDD Automation

In  [The Purposes of TDD](2022-05-13 The Purposes of TDD.md) I asked:

>  if you hired an assistant to manually test your code during development, how much time would you save?  A follow up question, if that assistant was a bot instead of a human, how much faster could the bot test a class 50 times or navigate web pages or fill out web forms 50 times?  This bot assistant would take seconds, while a human would take minutes each time.

A critical part of TDD includes automatically running test while writing code.  A test bot. This allows you to move much faster and identify problems quickly.

## Guard

[Guard](https://github.com/guard/guard) is a command line tool to easily handle events on file system modifications.  Guard comes with additional plugins for various automation runners.  For example, [Guard::RSpec](https://github.com/guard/guard-rspec) automatically runs your specs (much like autotest)

## Rubymine Auto Test

In RubyMine, you can enable the [autotest-like runner](https://www.jetbrains.com/help/ruby/performing-tests.html#run-tests-automatically): any test in the current run configuration restarts automatically after you change the related source code.

Using tools like this, you can run tests automatically while coding.

![tdd-auto-test](assets/2022-05-18 TDD Automation/tdd-auto-test.gif)

---

#TDD

---

- [2022-05-13 The Purposes of TDD](2022-05-13 The Purposes of TDD.md) 
- [GitHub - guard/guard: Guard is a command line tool to easily handle events on file system modifications.](https://github.com/guard/guard)
- [GitHub - guard/guard-rspec: Guard::RSpec automatically run your specs (much like autotest)](https://github.com/guard/guard-rspec)
- [Run tests | RubyMine](https://www.jetbrains.com/help/ruby/performing-tests.html#run-tests-automatically)
