# 2022-05-13 The Purposes of TDD

If you believe TDD is all about code coverage, then you are missing the point.  

When done correctly, you will prefer the TDD approach.  When done incorrectly, you will hate TDD like [this guy](https://charleeli.medium.com/why-tdd-is-bad-and-how-to-improve-your-process-d4b867274255#:~:text=TDD%20is%20Time%20Consuming%20and,test%20cases%20are%20code%2C%20too).

> TDD Focus on Passing Test, Not Correctness

His team is not leveraging the benefits of TDD, they are only focusing on code coverage.  If TDD was just about code coverage, but it's then it wouldn't matter if you wrote tests first or last so long as you got coverage.

> TDD is Time Consuming and Costly, in both Short Term and Long Term

They are either testing wrong, or they created a slow testing architecture.  When done correctly, TDD can actually speed up building software.

Note: I used to think the same way this guy did years ago, until I changed my focus of TDD, put some disciplines in place and became better as I practiced.

## Test Last

The number one benefit of TDD is better code.  If you test code in a way that you want to call your code (the way you would want to call your code), you write better.

Let's assume you wrote a class to find and return a book by an ID.

```ruby
class Book
  def find(id)
    ...
  end
end
```

To test or use the code, you would then use something like:

```ruby
book = Book.new.find(123)
```

This reads:

> book equals book new find 123

Hm, "book new find" reads odd. It also seems odd that I first create a `new` instance just to find a 2nd `instance` when no initial parameters are passed.  New does nothing bug create a class to immediately call a seond method.  What if we passed the ID to the new instead of find?

```ruby
book = Book.new(123).find
```

This reads:

> book equals book new 123 find

The `new` method makes sense because I'm using it to pass params to build an object, but it looks as if Yoda wrote it.  Lets step back and pretend the class does not yet exist.  How would I want the codes interface to read?

## TDD Primary Benefit:  Clean Code

### Easier To Read

In TDD wirting the test first is often refered to as "Write The Code You Wish You Had".  If I were to have started with the test first, I may have written it like this from the beginning.  This is the way in which I'd like to call the API.

```ruby
book = Book.find(123)
```

This reads:

> book equals book find 123

This code to call the class is simpler and reads much better.  Now, with the code like this, I check to see if I can write code to support the way I want to call the code. I write the class as:

```ruby
class Book
  def self.find(id)
   	...
  end
end
```

My class does not look much different than before, but the way I call is now reads better.

If your code reads like a book, then there you do not need to document as much.  Some documentation helps, but not as much.

And, since we read code much more than we write code, taking the time to write readable code is worth the effort in the long run.

### Improved Composition Emergence

In other scenarios with a TDD approach you write less decoupled code because it must work with the unit tests written.  Simpler, more *open/closed* class APIs emerge from this approach which then makes it easier to test, easier to refactor if needed and easier to create *[adapters](https://refactoring.guru/design-patterns/adapter)* and *[compose](https://refactoring.guru/design-patterns/composite)* objects together.



Overall, use TDD to help you write better, cleaner code... not just to "test" your code. 

## TDD Secondary Benefit: Speed

Note that articles online against TDD are also against testing in general.  They dislike testing, they feel it's a waste of time.  Again, it's because their perspective is often wrong or they are going about it wrong.

Ask yourself, if you hired an assistant to manually test your code during development, how much time would you save?  A follow up question, if that assistant was a bot instead of a human, how much faster could the bot test a class 50 times or navigate web pages or fill out web forms 50 times?  This bot assistant would take seconds, while a human would take minutes each time.

You need to write the tests anyway.  When you write tests at the end, you only get the benefit of it running in the CI pipeline.  It might save you time in the future, but doesn't save you time in initial development.  If you incrementally write out tests while you are building code, you get the benefit of an extremely fast testing assistant and your speed of development moves faster.

### Easier to Test

Another speed benefit is in writing the actual test.  When developers write the main code first and tests secondary, it often gives a feeling of fighting with tests which slows down the process and can lead to developer distaste for writing tests.  When using TDD, much of the struggle melts away and the process becomes easier and faster.



## TDD Side Benefit: Code Coverage

Code coverage for future refactoring is only a side benefit of TDD, but should not be our primary purpose of tests.  



## Summary

If the only purpose of TDD was code coverage, then it wouldn't matter if you wrote tests first or last, so long as you write them.  When done correctly TDD provides code coverage PLUS additional benefits for less time.



---

#tdd #test #cleancode

---

- [Why TDD is Bad (and How to Improve Your Process) | by Charlee Li | Medium](https://charleeli.medium.com/why-tdd-is-bad-and-how-to-improve-your-process-d4b867274255#:~:text=TDD%20is%20Time%20Consuming%20and,test%20cases%20are%20code%2C%20too)
- [OO - Composite](https://refactoring.guru/design-patterns/composite)
- [Adapter](https://refactoring.guru/design-patterns/adapter)
