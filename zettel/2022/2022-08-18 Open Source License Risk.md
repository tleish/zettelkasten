

# 2022-08-18 Open Source License Risk

Using open source for building software is critical.  It provides you some large shortcuts allowing you to build the software more quickly.  However, some licenses can create potential legal issues or may require you to open source your commercial software.  

![img](assets/2022-08-18 Open Source License Risk/62dcdc78664b19d7c5523cb1_open-source-licenses-comparison.png)

Focus on building software with open-source licenses in a *low* risk category.

A good tool for auditing project licenses is [LicenseFinder](https://github.com/pivotal/LicenseFinder) gem.  It can scan and report licenses from `bundler`, `yarn` and more.

## SVG Images/Icons and Fonts

Review licenses for SVG Images/Icons and fonts.  Common SVG licenses include

### RISK: LOW

- Apache License Version 2.0
- Public Domain
- Open Source
- MIT License
- SVG Repo License (default SVG Repo licensing if not specified on icon)

### RISK: HIGH

- GNU Public License
- Creative Commons
- Attribution-NonCommercial CC BY-NC

---

#license

---

- [Open Source Licenses to Avoid - Steps to Prevent the Legal Risk](https://brainhub.eu/library/open-source-licenses-to-avoid)
- [Top open source licenses and legal risk | Synopsys](https://www.synopsys.com/blogs/software-security/top-open-source-licenses/)
- [Open Source Licenses: Types and Comparison | Snyk](https://snyk.io/learn/open-source-licenses/)
- [LicenseFinder: Find licenses for your project's dependencies.](https://github.com/pivotal/LicenseFinder)
- [Open Source Licenses Demystified | Toptal](https://www.toptal.com/open-source/developers-guide-to-open-source-licenses)
