# 2022-05-12 Hotwire Progressive Enhancement

Definition of Progressive Enhancement from [MDN Web Docks](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement):

> Progressive enhancement is a design philosophy that provides a baseline of essential content and functionality to as many users as possible, while delivering the best possible experience only to users of the most modern browsers that can run all the required code.
> 
> The word progressive in progressive enhancement means creating a design that achieves a simpler-but-still-usable experience for users of older browsers and devices with limited capabilities, while at the same time being a design that progresses the user experience up to a more-compelling, fully-featured experience for users of newer browsers and devices with richer capabilities.

The resilient approach (or progressive enhancement) from the [hotwire/stimulus handbook](https://stimulus.hotwired.dev/handbook/designing-for-resilience):

> This resilient approach, commonly known as *progressive enhancement*, is the practice of delivering web interfaces such that the basic functionality is implemented in HTML and CSS, and tiered upgrades to that base experience are layered on top with CSS and JavaScript, progressively, when their underlying technologies are supported by the browser.

Sam Stephenson, Hotwire creator/contributor

> If the web offers us guidance, take it. The web gives us `<a>` to change pages and `<form>` to change server-side state, so that’s what we’ll use in our app. That way we have fewer decisions to make and we’re responsible for less code.
>
> If we want to do something that can’t be done with HTML and CSS alone, we’ll still try to do as much as we can with HTML and CSS, and use a layer of JavaScript on top for the rest.
>

## Progress Enhancements

1. Build the application to function without JavaScript
2. Enhance the user experience by responding to HTTP request with turbo-frames or turbo-streams
3. Enhance the user experience by "sprinkling" the application with a bit of JavaScript stimulus controllers
4. Enhance the user experience by updating the page using web socket turbo-streams

On some pages, level 1 is enough.  On other pages you might go all the way to level 4.  In each case, you start with level 1 and work/evaluate each level, not the other way around.

### Level 1: Write application to function without JavaScript

Write an application using backend, HTML and CSS only.   The experience of users without JavaScript is not ideal.  It might mean the UI does not match the original specs, the user must perform a few extra clicks, or a small menu might fill the entire page.  All of this is okay, so long as I can accomplish the tasks with a basic rails app.

Consider disabling JavaScript to ensure the experience works without JavaScript.  I like to use Chrome Web Developer plugin to do this.

### Level 2: Enhance the user experience by responding to HTTP request with turbo-frames or turbo-streams

Instead of jumping strait to writing complex JavaScript to update portions of the page as a user clicks, consider if it might be better to leverage the backend to enhance the users experience.

### Level 3: Enhance the user experience by "sprinkling" the application with a bit of JavaScript stimulus controllers

After we've enhanced the page with turbo-frames/streams (or at least evaluated turbo-frames/streams), consider improving the user experience with a small amount of JavaScript. 

### Level 4: Enhance the user experience by updating the page using web socket turbo-streams

You cannot accomplish some functionality with the previous tools discussed.  This is when you look to web sockets and turbo-streams to broadcast content to web clients.

## Benefit

The benefit of following this pattern means:

1. Simpler application
2. Faster application
3. Business logic on the backend
4. Less brittle font-end

---

#hotwire

---

-  [[Dreaming Bigger with Rails] David Heinemeier Hansson answers our questions about Hotwire  | Blog WizVille](https://www.wizville.com/en/blog/dhh-interview-hotwire)
-  [Hotwire vs htmx - comparison - #2 by sam - Hotwire Discussion](https://discuss.hotwired.dev/t/hotwire-vs-htmx-comparison/1614/2)
-   [Progressive Enhancement - MDN Web Docks](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement)
