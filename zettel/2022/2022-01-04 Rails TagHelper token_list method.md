# 2022-01-04 Rails TagHelper token_list method

With Hotwired data attributes and Tailwind CSS utility classes, html string interpolation can become messy and less readable.

```erb
<li id="<%= dom_id(@task) %>" 
    data-controller="class-toggle <%= 'modal' if @task.use_modal? %>">
  Item
</li>
```

If `#use_modal?` is false, it outputs the following HTML.

```erb
<li id="task-123" data-controller="class-toggle ">
  Item
</li>
```

### Extra Spaces

Notice the extra space in the `data-controller` value.  Browsers correctly handle this, but not always clean HTML.  We could fix this by moving the space to inside the quoted strings.

```erb
<li id="<%= dom_id(@task) %>" 
    data-controller="class-toggle<%= ' modal' if @task.use_modal? %>">
  Item
</li>
```

If `#use_modal?` is false, it outputs the following HTML.

```erb
<li id="task-123" data-controller="class-toggle">
  Item
</li>
```
This works, but the erb is uglier.

### token_list

A "token list" is a *space* separated list of items.  You can use [ActionView::Helpers::TagHelper#token_list(*args)](https://api.rubyonrails.org/classes/ActionView/Helpers/TagHelper.html#method-i-token_list) to easily builds a list of spaced values

```ruby
token_list("foo", "bar")
 # => "foo bar"
token_list("foo", "foo bar")
 # => "foo bar"
token_list({ foo: true, bar: false })
 # => "foo"
token_list(nil, false, 123, "", "foo", { bar: true })
 # => "123 foo bar"
```

### token_list and attribute values

Use `#token_list` method to build conditional attribute values.

```erb
<li id="<%= dom_id(@task) %>" 
    data-controller="<%= token_list(['class-toggle', { modal: @task.use_modal? }]) %>">
	Item
</li>
```
If `#use_modal?` is *false*, it outputs the following HTML.

```erb
<li id="task-123" data-controller="class-toggle">
  Item
</li>
```

Simplified erb and clean HTML attributes without spaces.

If `#use_modal?` is *true*, it outputs the following HTML.

```erb
<li id="task-123" data-controller="class-toggle modal">
  Item
</li>
```



### class_names

`#class_names` is an alias of `#token_list`

```diff
<li id="<%= dom_id(@task) %>" 
-   class="<%= token_list(['task', { 'text-red-500': @task.overdue?, 'text-gray-500':  @task.complete? }]) %>">
+   class="<%= class_names(['task', { 'text-red-500': @task.overdue?, 'text-gray-500':  @task.complete? }]) %>">
  Item
</li>
```
If the task is complete, it outputs the following HTML.

```erb
<li id="task-123" class="task text-gray-500"></li>
```

### tag and class_names

You can omit `class_names` when using the `class` attribute on a [TagHelper](2022-01-03 Simplify View String Interpolation with Rails ActionView::Helpers::TagHelper.md) as it uses `#class_names` by default.

```diff
<%= tag.li id: dom_id(@task),
-   class: class_names(['task', { 'text-red-500': @task.overdue?, 'text-gray-500':  @task.complete? }]) do %>
+   class: ['task', { 'text-red-500': @task.overdue?, 'text-gray-500':  @task.complete? }] do %>
  Item
</li>
```

---

#ruby #rails #actionview #erb #css

---

- [#token_list](https://api.rubyonrails.org/classes/ActionView/Helpers/TagHelper.html#method-i-token_list)
- [Introducing the Rails class_names method](https://dev.to/drbragg/introducing-the-rails-classnames-method-2bdm)
- [Simplify View String Interpolation with Rails ActionView::Helpers::TagHelper](2022-01-03 Simplify View String Interpolation with Rails ActionView::Helpers::TagHelper.md) 
