

# 2022-02-10 HTML input[form] Attribute

When rendering a form on the backend, sometime you want to position a button outside the form in the HTML in order to position the search button in a different part of the page. 

```html
<header>
  <form id="blog_search" action="/blogs" method="GET">
	  <input type="search" name="query" value="" />
	</form>
</header>

<aside>
  <input type="submit" name="search" value="Search" />
</aside>
```

The above will not work.

Normally you might think to write some Javascript 

```html
<input type="submit" name="search" value="Search" onclick="document.getElementById('blog_search').submit()" />
```

However, there's a simpler HTML alternative that requires no Javascript 🎉.


## input[form]

Add the HTML5 `input[form]` attribute to target a form by id.

```diff
<aside>
- <input type="submit" name="search" value="Search" />
+ <input type="submit" name="search" value="Search" form="blog_search" />
</aside>
```

This can be used with any type of `input` tag (not just buttons).

```html
<header>
  <form id="blog_search" action="/blogs" method="GET"></form>
</header>

<aside>
	<input type="search" name="query" value="" form="blog_search" />
  <input type="submit" name="search" value="Search" form="blog_search" />
</aside>
```




## button[form]

You can also use `button[form]` attribute to target a form by id.

```diff
<aside>
- <input type="submit" name="search" value="Search" />
+ <button type="submit" name="search" value="Search" form="blog_search" />
</aside>
```



## Avoid Nesting Forms

Sometimes we also consider to "nest" forms because of HTML rendering partials, but this goes against [html5 best practice](https://www.w3.org/TR/html5/forms.html#the-form-element):

> **4.10.3 The `form` element**
>
> **Content model:**
>
> Flow content, but with no form element descendants.

Use `input[form]` to avoid nesting forms.

## Hotwire

This approach works really well with the Hotwire framework when you can have snippets of html updates on the page using turbo-frame or turbo-stream.

---

#html #forms

---

- [HTML input form Attribute](https://www.w3schools.com/tags/att_input_form.asp)
- [StackOverflow](https://stackoverflow.com/questions/379610/can-you-nest-html-forms#answer-28528359)
- [Dynamically change Form Action or Method using HTML.md](2022-02-16 Dynamically change Form Action or Method using HTML.md) 

