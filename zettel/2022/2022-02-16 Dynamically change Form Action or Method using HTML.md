

# 2022-02-16 Dynamically change Form Action or Method using HTML

When submitting a form, sometimes you may want to use the same form but with different action or method.  

```html
<form action="/issues" method="POST">
  <!-- your form inputs here -->

  <input type="submit" value="Submit" />
  <input type="submit" value="Search for similar issues" onclick="searchIssues">
</form>
```

In the above example, a user can enter details for an issue to submit the issue.  If we also wanted a button that allowed the users to take the same parameters from the form and send them to a different endpoint normally would reach for javascript to collect all the for input values and send to the alternative endpoint.

However, there's a native HTML, non-javascript way to accomplish this. 🎉

## [formaction] and [formmethod] attributes

You can do this with just HTML using attributes`formaction` and/or `formmethod` on `input` or `button` elements.

```diff
<form action="/issues" method="POST">
  <!-- your form inputs here -->

  <input type="submit" value="Submit" />
- <input type="submit" value="Search for similar issues" onclick="issueSearch">
+ <input type="submit" value="Search for similar issues" formaction="/issues/search" formmethod="GET">
</form>
```

🤯

You can use just `formaction` or just `formmethod`, or both.  Combine this with [input[form]](2022-02-10 HTML input[form] Attribute.md) attribute and you have all sorts of options for dynamically submitting forms.


---

#html #forms

---

- [2022-02-10 HTML input[form] Attribute](2022-02-10 HTML input[form] Attribute.md) 
- [The Input (Form Input) element - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attr-formaction)
- [HTML input formaction Attribute](https://www.w3schools.com/tags/att_input_formaction.asp)
- [HTML input formmethod Attribute](https://www.w3schools.com/tags/att_input_formmethod.asp)
- [2022-02-17 Skip Form Validation.md](2022-02-17 Skip Form Validation.md) 

