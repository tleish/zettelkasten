

# 2022-05-16 The Three Laws of TDD

Writing code using TDD initially takes discipline and patience, but if you understand [the primary purpose of TDD](2022-05-12 The Primary Purpose of TDD.md)  then after a short while it becomes the preferred approach.  Kent Beck wrote a book [Test Driven Development: By Example](https://www.oreilly.com/library/view/test-driven-development/0321146530/) and said the following about the correct approach.

> Make it run.  Quickly getting that bar to go to green dominates everything else. If a clean, simple solution is obvious, then type it in. If the clean, simple solution is obvious, but it will take you a minute, then make a note of it and get back to the main problem, which is getting the bar green in seconds.  

Kent goes on to say:

> This shift in aesthetics is hard for some experienced software engineers.

I found this to be true myself.  It's a change in coding flow that I was not used to, but once I updated my flow I prefered the TDD flow over the previous flow. 

## The Three Laws of TDD

To learn the "simple solution" approach mentioned by Kent Beck, follow these Three Laws of TDD.

1. You are not allowed to write any production code unless it is to make a failing unit test pass.
2. You are not allowed to write any more of a unit test than is sufficent to fail;  and compliation failures are failures.
3. You are not allowed to write any more production code than is sufficient to pass the one failing unit test.

Watch: [YouTube - The Three Laws of TDD (Featuring Kotlin)](https://www.youtube.com/watch?v=qkblc5WRn-U)

Read: [I was doing TDD wrong and you (probably) are | Ibrahima Ciss](https://iciss.dev/articles/i-was-doing-tdd-wrong-and-you-probably-are)

## Unit vs System Tests

Note that these TDD rules primarily apply to "unit test".  I still suggest using them for System tests

---

#TDD

---

- [2022-05-13 The Purposes of TDD](2022-05-13 The Purposes of TDD.md) 
- [Test Driven Development: By Example [Book] - Kent Beck](https://www.oreilly.com/library/view/test-driven-development/0321146530/)
- [The Three Laws of TDD (Featuring Kotlin)](https://www.youtube.com/watch?v=qkblc5WRn-U)
- [ArticleS.UncleBob.TheThreeRulesOfTdd](http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd)
- [I was doing TDD wrong and you (probably) are | Ibrahima Ciss](https://iciss.dev/articles/i-was-doing-tdd-wrong-and-you-probably-are)
