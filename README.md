# Engineering zettelkasten

A [zettelkasten](https://zettelkasten.de/posts/overview/) is a simple method for capturing *small* thoughts/notes.  Creating this zettelkasten to capture ideas to share with engineering teams.



## IDEAS

- TailwindCSS

  - 

- Turbo
  - XHR Redirects
  - Turbo Frames
  - Turbo Streams
  - Turbo Frames vs Turbo Streams
  - New Features
  
- FOUC
  - bfcache
  - form_controller
  - fonts
  - js vs no-js
  
- Stimulus
  - Generic Controllers
  - dispatch events
  - Callback memory leak
  - ObserveMutations
  
- Caching
  - Turbo
  - eTags
    - eTags on pages with additional queries (select list)
  - Russian Doll
  
- Javascript

  - [GitHub - you-dont-need/You-Dont-Need-Momentjs: List of functions which you can use to replace moment.js + ESLint Plugin](https://github.com/you-dont-need/You-Dont-Need-Momentjs)
  - You Might Not Need jQuery
    - [Are you sure you need jQuery?](https://dev.to/oinak/are-you-sure-you-need-jquery-ej8)
    - [You Don’t Need jQuery](https://dev.to/kdinnypaul/you-don-t-need-jquery-1a18)
    - [You Might Not Need jQuery](http://youmightnotneedjquery.com/)

- Components

- Ruby
  - Rules to Follow
  
  - AR Callbacks 
  
    ```ruby
    msg = Message.new
    class << msg
      before_save -> { puts "Message #{self} is saved" } # Here, `self` is the msg instance
    end
    Message.before_save # Calling this with no args will ensure that it gets added to the callbacks chain (but only for your instance)
    ```
  
  - [Rails Callbacks: 5 Best Practices Used at Gusto](https://engineering.gusto.com/the-rails-callbacks-best-practices-used-at-gusto/)
  
  - AR Scopes: [Filterrific - A Rails Engine plugin that makes it easy to add filtering, searching, and sorting to your ActiveRecord lists.](http://filterrific.clearcove.ca/pages/active_record_scope_patterns.html)
  
  - AR Validation
  
- Testing

  - Code Coverage
  - Manually Test layout and interactions on Mobile

- Rails

  - Unit Tests
  - [[N+1 is a Rails feature - rossta.net](https://rossta.net/blog/n-1-is-a-rails-feature.html)]([N+1 is a Rails feature - rossta.net](https://rossta.net/blog/n-1-is-a-rails-feature.html))

- General Engineering
  - Webhooks
  - [Let’s Get Serious About Readability](https://medium.com/newsonthebloc/lets-get-serious-about-readability-4e4ce6a9b6c2)
  - Null Object Pattern (https://gitlab.com/shipninja/web/-/merge_requests/88/diffs)
  - [.gitignore mistake that everyone makes - DEV Community](https://dev.to/gajus/gitignore-mistake-that-everyone-makes-44kb)
  
- Design
  - SVG Icons
  - SVG Optimization
  - [Infinite Scrolling, Pagination Or “Load More” Buttons? Usability Findings In eCommerce — Smashing Magazine](https://www.smashingmagazine.com/2016/03/pagination-infinite-scrolling-load-more-buttons/)
  - Fonts
  - FOUC Fonts



---

#todo

---

- [Building A Zettelkasten In Typora - by Stowe Boyd](https://www.workfutures.io/p/building-a-zettelkasten-in-typora)





